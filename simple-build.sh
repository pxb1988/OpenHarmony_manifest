#!/bin/bash

set -ex

if [ ! "$(which podman)" = "" ] ;then
  echo "已经安装了podman"
  DOCKER=podman
elif [ ! "$(which docker)" = "" ] ;then
  echo "已经安装了docker"
  DOCKER=docker
elif [ "$UID" = "0" ]; then
  echo "尝试安装docker, 请稍等"
  curl -fsSL https://get.docker.com -o get-docker.sh
  sh get-docker.sh --mirror Aliyun
  DOCKER=docker
else
  echo "没有找到docker, 请安装docker"
  exit 1
fi

$DOCKER run -ti --rm -v `pwd`:/home/openharmony swr.cn-south-1.myhuaweicloud.com/openharmony-docker/docker_oh_standard:3.2 bash -c "
set -ex
mkdir -p bin src
curl https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 -o bin/repo
chmod a+x bin/repo
"

$DOCKER run -ti --rm -v `pwd`:/home/openharmony swr.cn-south-1.myhuaweicloud.com/openharmony-docker/docker_oh_standard:3.2 bash -c "
set -ex
cd src
../bin/repo init -u https://gitee.com/pxb1988/OpenHarmony_manifest.git -b OpenHarmony-4.1-Release --no-repo-verify -m last-working-opi5plus-ohos-4.1.xml -q < /dev/null
../bin/repo sync -c --force-sync
../bin/repo forall -c 'git lfs pull'
bash build/prebuilts_download.sh
"

$DOCKER run -ti --rm -v `pwd`:/home/openharmony swr.cn-south-1.myhuaweicloud.com/openharmony-docker/docker_oh_standard:3.2 bash -c "
set -ex
export CCACHE_BASE=/home/openharmony/ccache
cd src
./build.sh --product-name opi5plus --ccache --no-prebuilt-sdk
"
