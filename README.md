

# 这个仓库用于编译`OrangePi 5 plus`的OpenHarmony固件镜像

# 这个仓库引用了
    - https://gitee.com/openharmony/manifest
    - https://gitee.com/ohos-porting-communities


是这个教程的一个简化版本：https://gitee.com/ohos-porting-communities/vendor_opc/blob/OpenHarmony-4.1-Release/README.md


# 使用方法

## 1 安装docker

```bash
   curl -fsSL https://get.docker.com -o get-docker.sh
   sudo sh get-docker.sh --mirror Aliyun
```

  更详细的信息在这里https://docs.docker.com/engine/install/

## 2 准备本地文件目录

```bash
    mkdir -p ~/oh

```

  计划目录结构
```
  oh/
    - bin/
      - repo
    - src/
      - .repo/
      - ... 其他
    - openharmony_prebuilts/
```

## 3 使用华为官方的docker镜像

```bash
    cd ~/oh
    docker run -ti --rm -v `pwd`:/home/openharmony swr.cn-south-1.myhuaweicloud.com/openharmony-docker/docker_oh_standard:3.2
```


## 4 下载源码和预编译

```bash
    mkdir -p bin src
    curl https://gitee.com/oschina/repo/raw/fork_flow/repo-py3 -o bin/repo
    chmod a+x bin/repo

    ( cd src
        ../bin/repo init -u https://gitee.com/pxb1988/OpenHarmony_manifest.git -b OpenHarmony-4.1-Release -m last-working-opi5plus-ohos-4.1.xml --no-repo-verify
        ../bin/repo sync -c --force-sync
        ../bin/repo forall -c 'git lfs pull'

        # 下载预编译
        bash build/prebuilts_download.sh
    )
```


## 5 编译

```bash
    cd src
    ./build.sh --product-name opi5plus --ccache --no-prebuilt-sdk

```
刷机包在out/opi5plus/packages/phone/images

## 6 刷机方法在这里

  https://gitee.com/hihope_iot/docs/blob/master/HiHope_DAYU200/docs/%E7%83%A7%E5%BD%95%E6%8C%87%E5%AF%BC%E6%96%87%E6%A1%A3.md

# 什么？还是嫌麻烦, 试试这个

```bash
  curl https://gitee.com/pxb1988/OpenHarmony_manifest/raw/OpenHarmony-4.1-Release/simple-build.sh -o simple-build.sh
  chmod a+x simple-build.sh
  ./simple-build.sh
```

# 出错了？

删除src目录下除了.repo之外的所有文件/文件夹，重新再运行一遍。

# 注意

默认编译的是`last-working-opi5plus-ohos-4.1.xml`， 可能不是最新的版本，你可以尝试在repo init时修改为 `-m chipsets/opi5plus.xml`编译最新的代码

